// Users Collection

{
    "_id":"user001",
    "firstName" : "John",
    "lastName" : "Daet",
    "email" : "johndaet@mail.com"
    "password" : "johndaet123",
    "isAdmin" : true,
    "mobileNumber" : "09123123123",
    "dateTimeRegistered" : "2022-06-10T15:00:00.00Z"

}

{
    "_id":"user002",
    "firstName" : "Jack",
    "lastName" : "Smith",
    "email" : "jacksmith@mail.com"
    "password" : "jacksmith123",
    "isAdmin" : false,
    "mobileNumber" : "09123456789",
    "dateTimeRegistered" : "2022-06-10T15:00:00.00Z"
}

// Products Collection

{
    "_id":"product001",
    "itemName" : "Wooden Chair",
    "description" : "Wooden Chair made from Narra tree",
    "price" : 500,
    "stocks" : 50,
    "isActive" : true,
    "sku" : "WCNT-001",

}

{
    "_id":"product002",
    "itemName" : "Iphone 13 Case",
    "description" : "Iphone 13 Case with different designs",
    "price" : 250,
    "stocks" : 50,
    "isActive" : true,
    "sku" : "IP13C-001",

}

// Order Products Collection

{
    "_id":"op001",
    "userId" : "user002",
    "productId" : "product001",
    "quantity" : 2,
    "price" : 500,
    "subTotal" : 1000,

}

{
    "_id":"op002",
    "userId" : "user002",
    "productId" : "product002",
    "quantity" : 3,
    "price" : 250,
    "subTotal" : 750,

}

// Orders Collection

{
    "_id":"order001",
    "userId" : "user002",
    "transactionDate" : "2022-06-10T15:00:00.00Z"
    "status" : [],
    "total" : 1750,


}